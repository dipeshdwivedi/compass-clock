import React, {useState, useEffect} from 'react';
import './App.css';


const app = () => {
    window.datew = new Date();
    const [year, setYear] = useState('');
    const [month, setMonth] = useState('');
    const [weekday, setWeekDay] = useState('');
    const [day, setDay] = useState('');
    const [hour, setHour] = useState('');
    const [minute, setMinute] = useState('');
    const [sec, setSec] = useState('');

    useEffect(() => {
        const interval = setInterval(() => {
            const date = new Date();
            setYear(date.getFullYear());
            setMonth(date.getMonth()+1);
            setWeekDay(date.getDay());
            setDay(date.getDate());
            setHour(date.getHours());
            setMinute(date.getMinutes());
            setSec(date.getSeconds() + 1);
        }, 1000);
        return () => {
            clearInterval(interval);
        };
    }, []);


    // return clock(props);
    var array = length => Array.from({length}).map((i, v) => v + 1);
    const daysInMonth = (_year,_month) => {
        return new Date(_year,_month,0).getDate();
    }
    const _day = daysInMonth(year,month);

    return (<div>
        <div className='year'
            style = {{
                color : '#891805',
            }}
            >{year}</div>
        <div className="Month">{array(12).map((i, v) => {
            return (<div className={"month"}>
                <div style={{
                    width: '210px',
                    background: '',
                    transform: 'rotate(' + ((30 * i) - (30 * month)) + 'deg)',
                    scale : (month == i ? '1.2' : ''),
                    color : (month == i ? '#891805' : ''),
                    transition  : 'transform .6s',


                }}>
                    {i} Month
                </div>
            </div>);
        })}
        </div>
        <div className="WeekDay">{array(7).map((i, v) => {
            return (<div className={"weekday"}>
                <div style={{
                    width: '410px',
                    background: '',
                    transform: 'rotate(' + ((51 * i) - (51 * weekday)) + 'deg)',
                    scale : (weekday == i ? '1.2' : ''),
                    color : (weekday == i ? '#891805' : ''),
                    transition  : 'transform .6s',


                }}>
                    WeekDay {i}
                </div>
            </div>);
        })}</div>
        <div className="Day">{array(_day).map((i, v) => {
            return (<div className={"day"}>
                <div style={{
                    width: '550px',
                    background: '',
                    transform: 'rotate(' + ((360/_day * i) - (360/_day * day)) + 'deg)',
                    scale : (day == i ? '1.2' : ''),
                    color : (day == i ? '#891805' : ''),
                    transition  : 'transform .6s',


                }}>
                    Day {i}
                </div>
            </div>);
        })}
        </div>
        <div className="Hour">{array(24).map((i, v) => {
            return (<div className={"hour"}>
                <div style={{
                    width: '730px',
                    background: '',
                    transform: 'rotate(' + (((15) * i) - ((15) * hour)) + 'deg)',
                    scale : (hour == i ? '1.2' : ''),
                    color : (hour == i ? '#891805' : ''),
                    transition  : 'transform .6s',


                }}>
                    {i} Hour{(i > 1) ? 's' : ''}
                </div>
            </div>);
        })}
        </div>
        <div className="Minute">{array(60).map((i, v) => {
            return (<div className={"minute"}>
                <div style={{
                    width: '930px',
                    background: '',
                    transform: 'rotate(' + (((6) * i) - ((6) * minute)) + 'deg)',
                    scale : (minute == i ? '1.2' : ''),
                    color : (minute == i ? '#891805' : ''),
                    transition  : 'transform .6s',


                }}>
                    {i} Minute{(i > 1) ? 's' : ''}
                </div>
            </div>);
        })}
        </div>
        <div className="Second">{array(60).map((i, v) => {
            return (<div className={"sec"}>
                <div style={{
                    width: '1090px',
                    background: '',
                    transform: 'rotate(' + (((6) * i) - ((6) * sec)) + 'deg)',
                    scale :(sec == i ? '1.2' : ''),
                    color : (sec == i ? '#891805' : ''),
                    transition  : 'transform .6s',
                }}>
                    {i} Sec{(i > 1) ? 's' : ''}
                </div>
            </div>);
        })}
        </div>
    </div>);

}
export default app;
